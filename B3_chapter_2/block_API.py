import API
from utils import linear_classify


def get_net_handle():
	return API.CatRabbitDetection()

def get_classifier():
	return linear_classify.LinearClassifier()

def load_image(path):
	return API.load_image(path)

def load_image_url(url):
	return API.load_image_url(url)

def load_folder_url(url):
	return API.load_folder_url(url)
	
def load_image_folder(path):
	return API.load_image_folder(path)

def imshow(img):
	return API.show(img)

def get_hog(img):
	return API.get_hog(img)

def show_hog(hog):
	return API.show_hog(hog)

def get_animal_feature(handle, img):
	if isinstance(handle, API.CatRabbitDetection):
		return handle.get_feature(img)
	else:
		raise ValueError("handle is not an instance of CatRabbitDetection!")

def combine_dataset(set1, set2):
	return set1 + set2

def train(handle, features, labels):
	if isinstance(handle, linear_classify.LinearClassifier):
		return handle.train(features, labels)
	else:
		raise ValueError("handle is not an instance of CatRabbitDetection!")

def predict(handle, feature):
	if isinstance(handle, linear_classify.LinearClassifier):
		return handle.pred([feature])
	else:
		raise ValueError("handle is not an instance of CatRabbitDetection!")

