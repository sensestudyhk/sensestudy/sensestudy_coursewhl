import os
import pickle
import cv2
from skimage import feature as ft
from ssplot import *
from utils import *
import urllib.request

BASE_PATH, MODEL_PATH, DATA_PATH = get_paths()


class CatRabbitDetection(MobileNetDetection):
    def __init__(self, thresh=0.7):
        super(CatRabbitDetection, self).__init__()

    def get_feature(self, img):
        all_hashed_feature = [feat[2:] for feat in get_dir(
            os.path.join(DATA_PATH, "hashed_feature"))]
        image_hash = get_img_hash(img)
        if image_hash in all_hashed_feature:
            with open(os.path.join(DATA_PATH, "hashed_feature", image_hash), 'rb') as f:
                loaded_feature = pickle.load(f)
                return loaded_feature
        else:
            # print("please wait...")
            self.detected_list = []
            self.cvNet.setInput(cv2.dnn.blobFromImage(
                img, size=(300, 300), swapRB=True, crop=False))
            cvOut = self.cvNet.forward(
                'FeatureExtractor/MobilenetV1/MaxPool2d_4_2x2/MaxPool')
            return cvOut.reshape(-1)


def load_image(path):
    path = os.path.join(DATA_PATH, path)
    img = cv2.imread(path)
    if img is None:
        print('error, cannot load picture at given location!')
    return img


def load_image_url(url):
    from urllib.parse import quote
    import string
    s = quote(url, safe=string.printable)
    resp = urllib.request.urlopen(s)
    image = np.asarray(bytearray(resp.read()), dtype="uint8")
    image = cv2.imdecode(image, cv2.IMREAD_COLOR)
    return image


def load_image_folder(path):
    path = os.path.join(DATA_PATH, path)
    imgs = []
    for root, dirs, files in os.walk(path, topdown=False):
        for name in files:
            impath = os.path.join(root, name)
            img = load_image(impath)
            img = cv2.resize(img, (300, 300), interpolation=cv2.INTER_AREA)
            imgs.append(img)
    if imgs is None:
        print('错误! 无法在该地址找到图片!')
    return imgs

def load_folder_url(url):
    from urllib.parse import quote
    import string
    imgs = []
    num = 1
    
    while True:
        try:
            txt = str(num) +'.jpg'
            num = num + 1
            img_url = quote(url, safe=string.printable) + '/' + txt
            resp = urllib.request.urlopen(img_url) 
        except:
            break
        image = np.asarray(bytearray(resp.read()), dtype="uint8")
        image = cv2.imdecode(image, cv2.IMREAD_COLOR)
        imgs.append(image)
        
    return imgs

def show(img):
    fig() + image(img[:, :, ::-1])


def get_hog(img):
    features, hog_img = ft.hog(
        img,  # input image
        orientations=9,  # number of bins
        pixels_per_cell=(10, 10),  # pixel per cell
        cells_per_block=(1, 1),  # cells per blcok
        block_norm='L1',  # block norm : str {‘L1’, ‘L1-sqrt’, ‘L2’, ‘L2-Hys’}
        # power law compression (also known as gamma correction)
        transform_sqrt=True,
        feature_vector=True,  # flatten the final vectors
        visualize=True)  # return HOG map
    return (features, hog_img)


def show_hog(hog):
    features, hog_img = hog
    fig() + image(hog_img*255)


if __name__ == "__main__":
    net_handle = CatRabbitDetection()

    imgcats = loadImgsFromFolder('L2/cat/')  # 读取猫的图片
    imgrabbits = loadImgsFromFolder('L2/rabbit/')  # 读取兔的图片

    show(imgcats[0])
    show(imgrabbits[5])
    ################

    imgs = imgcats+imgrabbits  # 这里+可以把两部分图片合在一起
    hog, features = [], []
    for i in imgs:
        img_hog = get_hog(i)
        img_feature = net_handle.get_feature(i)
        hog.append(img_hog)
        features.append(img_feature)

    labels = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
              0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1]

    handle = LinearClassifier()
    handle.train(features, labels)

    imgs = load_image_folder("test")

    for i in imgs:
        feature = feature_handle.get_feature(i).reshape(1, -1)
        res = handle.pred(feature)
        if res[0] == 0:
            print("这是一只猫")
        else:
            print("这是一只兔子")
