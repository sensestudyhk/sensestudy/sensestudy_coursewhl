from .filename_handle import *
import os
import cv2


BASE_PATH, MODEL_PATH, DATA_PATH = get_paths()

model_path = os.path.join(MODEL_PATH, 'frozen_inference_graph.pb')
config_path =os.path.join(MODEL_PATH, 'ssd_mobilenet_v1_ppn_shared_box_predictor_300x300_coco14_sync.pbtxt')

with open(os.path.join(MODEL_PATH, "label_list.txt"), "r") as f:
    content = f.readlines()
    label_list = [line.strip() for line in content]

class DetectedObject:
    def __init__(self, detection):
        self.result = int(detection[1])-1
        self.name = label_list[int(detection[1])-1]
        self.left = detection[3]
        self.top = detection[4]
        self.right = detection[5]
        self.bottom = detection[6]


class MobileNetDetection:
    def __init__(self, model_path=model_path, config_path=config_path, thresh=0.7):
        self.thresh = 0.7
        self.detected_list = []
        self.cvNet = cv2.dnn.readNetFromTensorflow(model_path, config_path)

    def detect(self, img):
        self.detected_list = []
        self.cvNet.setInput(cv2.dnn.blobFromImage(img, size=(300, 300), swapRB=True, crop=False))
        cvOut = self.cvNet.forward()

        for detection in cvOut[0,0,:,:]:
            score = float(detection[2])
            if score > self.thresh:
                self.detected_list.append(DetectedObject(detection))
        return self.detected_list

def draw_obj_rect_on_image(detected_list, img, color=(23, 230, 210), thickness=2):
    rows = img.shape[0]
    cols = img.shape[1]
    copied_img = img.copy()
    for rec in detected_list:
        cv2.rectangle(copied_img, (int(rec.left*cols), int(rec.top*rows)), (int(rec.right*cols), int(rec.bottom*rows)), color, thickness)
        cv2.putText(copied_img, rec.name, (int(rec.left*cols), int(rec.top*rows)-10), cv2.FONT_HERSHEY_SIMPLEX, 1.0, color, thickness)  
    return copied_img

