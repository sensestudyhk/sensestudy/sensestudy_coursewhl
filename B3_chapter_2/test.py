import traceback
from block_API import *
	
def print_case(label):
    case = [
        "cat",
        "dog",
        "panda",
        "rabbit"
    ]
    print (case[label])

try:
    test_cat = load_image_folder('cat')

    test_dog = load_image_folder('dog')

    test_panda = load_image_folder('panda')

    test_rabbit = load_image_folder('rabbit')

    feature_net = get_net_handle()
    features = []
    labels = []
    for item in test_cat:
        img_feature = get_animal_feature(feature_net, item)
        features.append(img_feature)
        labels.append(0) # 對hamster圖片進行標簽
        
    for item in test_dog:
        img_feature = get_animal_feature(feature_net, item)
        features.append(img_feature)
        labels.append(1) # 對hamster圖片進行標簽

    for item in test_panda:
        img_feature = get_animal_feature(feature_net, item)
        features.append(img_feature)
        labels.append(2) # 對potato的圖片進行標簽
    
    for item in test_rabbit:
        img_feature = get_animal_feature(feature_net, item)
        features.append(img_feature)
        labels.append(3) # 對potato的圖片進行標簽

    classifier = get_classifier()
    train(classifier, features, labels)
        

    test_imgs = load_image_folder('test')
        
    for item in test_imgs:
        #imshow(item)
        img_feature = get_animal_feature(feature_net, item)
        res = predict(classifier, img_feature)
        #print(res)
        print_case(int(res))
        
except:
    print('error')
    print(traceback.format_exc())