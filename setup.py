from setuptools import find_packages, setup
import os


def package_files(directory):
    paths = []
    for (path, directories, filenames) in os.walk(directory):
        for filename in filenames:
            paths.append(os.path.join('..', path, filename))
    return paths


extra_files = package_files('.')
print(extra_files)

setup(
    name='Book3_A_Customized_no_3A',
    version='0.0.6',
    description='Book3_A_Customized_no_3A',
    keywords='Book3_A_Customized_no_3A',
    packages=find_packages(),
    include_package_data=True,
    package_data={'': extra_files},
)

# run with
# python setup.py bdist_wheel
