from .filename_handle import *
import numpy as np 
import hashlib

def get_img_hash(img):
	md5 = hashlib.md5()
	md5.update(str(img).encode('utf-8'))
	return str(md5.hexdigest())

def get_dir(dir):
	data = []
	for root, dirs, files in os.walk(dir):
		rel_root = os.path.relpath(root, dir)
		for name in files:
			if name.endswith('.py'):
				continue
			data.append(os.path.join(rel_root, name))
	return data
