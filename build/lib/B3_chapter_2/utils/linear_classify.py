import numpy as np
from sklearn.svm import LinearSVC


class LinearClassifier():
	def __init__(self):
		self.cls = LinearSVC()
		self.x = None
		self.y = None

	def train(self, x, y):
		self.x = x
		self.y = y
		self.cls.fit(x, y)

	def pred(self, x):
		predict = self.cls.predict(x)
		return predict

	def get_weights(self):
		k = self.cls.coef_
		b = self.cls.intercept_
		return k, b


